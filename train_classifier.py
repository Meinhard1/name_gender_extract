'''
Build a gender classifier based on names using the NLTK library
'''

import nltk
import pandas as pd
from nltk.classify import accuracy
import pickle
import requests
import io


def gender_features(name):
    """
    Extract features from name
    :param name: name to extract features from
    :return: features
    """
    features = {}
    features["last_letter"] = name[-1].lower()
    features["first_letter"] = name[0].lower()
    # names ending in -yn are mostly female, names ending in -ch are mostly male, so add 3 more features
    features["suffix2"] = name[-2:]
    features["suffix3"] = name[-3:]
    features["suffix4"] = name[-4:]
    return features


def format_data(df):
    """
    Format data for training
    :param df: dataframe to format
    :return: formatted data
    """
    records = df.to_records(index=False)
    result = list(records)
    return result


# Read the names from the files.
data_url = 'https://raw.githubusercontent.com/ellisbrown/name2gender/master/data/name_gender_data.csv'
response = requests.get(data_url)
df_names = pd.read_csv(io.StringIO(response.text), header=None)
df_names.columns = ['name', 'gender']
df_names = df_names.dropna()
print(len(df_names))

# Format
format_names = format_data(df_names)
featuresets = [(gender_features(n), g) for (n, g) in format_names]

# split train and test
list_train = featuresets[:100000]
list_test = featuresets[100000:]

# train
classifier = nltk.NaiveBayesClassifier.train(list_train)

# Predict
print(classifier.classify(gender_features('Ben')))

# check accuracy
print(accuracy(classifier, list_test)) # 0.85

# Finally, we can examine the classifier to determine which features it found
# most effective for distinguishing the names' genders:
classifier.show_most_informative_features(15)

'''
with a accuracy of 85% it not bad, lets train on full set and save
'''
# train
classifier = nltk.NaiveBayesClassifier.train(featuresets)

f = open('./name_gender_classifier.pickle', 'wb')
pickle.dump(classifier, f)
f.close()
