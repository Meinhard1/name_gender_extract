# Created by meinhardmare at 2020-07-28
'''
This create a skeleton  for scoring a model on a new dataset
'''

import pickle
import pandas as pd

def gender_features(name):
    """
    Extract features from name
    :param name: name to extract features from
    :return: features
    """
    features = {}
    features["last_letter"] = name[-1].lower()
    features["first_letter"] = name[0].lower()
    # names ending in -yn are mostly female, names ending in -ch are mostly male, so add 3 more features
    features["suffix2"] = name[-2:]
    features["suffix3"] = name[-3:]
    features["suffix4"] = name[-4:]
    return features

def predict_gender(name):
    """
    Predict gender from name
    :param name:
    :return: gender
    """
    gender = classifier.classify(gender_features(name))
    return gender

# Get data
df = pd.read_csv()

# load model
f = open('name_gender_classifier.pickle', 'rb')
classifier = pickle.load(f)
f.close()

# Predict
df['gender'] = df['first_name'].apply(predict_gender)
