# Gender classifier based on the first few letters of a name

I came across an interesting article that discussed predicting a person's gender based on their name and the 
first few letters of it. Intrigued by this idea, I decided to explore the possibility of building a Bayesian model 
to achieve this. My goal was to assess the accuracy of the results obtained from this model.

In Chapter 4 of the NLTK book, we saw that male and female names have some distinctive characteristics. 
Names ending in a, e and i are likely to be female, while names ending in k, o, r, s and t are likely to be male. 
Let's build a classifier to model these differences more precisely.

Source: https://www.kaggle.com/code/alvations/gender-identification-nltk-book-6-1-1-6-1-2/notebook

## Data
found this cool name gender training set
https://raw.githubusercontent.com/ellisbrown/name2gender/master/data/name_gender_data.csv

### Model
To save model:
```python
    import pickle
    f = open('my_classifier.pickle', 'wb')
    pickle.dump(classifier, f)
    f.close()
```
    
To load later:
```python
    import pickle
    f = open('my_classifier.pickle', 'rb')
    classifier = pickle.load(f)
    f.close()
```
